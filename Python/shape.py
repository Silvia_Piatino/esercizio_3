from math import pi
from math import sqrt


class Point:
    def __init__(self, x: float, y: float):
        self.ascissa = x
        self.ordinata = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.centro = center
        self.semiasseMaggiore = a
        self.semiasseMinore = b

    def area(self):
        return self.semiasseMaggiore * self.semiasseMinore * pi


class Circle(Ellipse): # nelle () ci metto la classe padre
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius) # chiamo il costruttore della classe padre in modo da considerare la classe circle come la classe ellipse, i cui semiassi valgono radius
    # non serve ridefinire l'area perché la eredita dal padre

# c = Circle(Point(0,0), 4)
# print(c.area())

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.punto1 = p1
        self.punto2 = p2
        self.punto3 = p3

    def area(self):
        return (abs((self.punto2.ascissa - self.punto1.ascissa) * (self.punto3.ordinata - self.punto1.ordinata) - (
                    self.punto2.ordinata - self.punto1.ordinata) * (self.punto3.ascissa - self.punto1.ascissa))) / 2


class TriangleEquilateral(Triangle): # conviene sovrascrivere i membri della classe padre; inoltre, se la classe padre identifica uno specifico triangolo, in una specifica posizione, invece la classe figlia identifica infiniti triangoli
    def __init__(self, p1: Point, edge: int): # dovresti assumere che è un particolare triangolo equilatero per usare il costruttore della classe padre
        self.punto = p1
        self.lato = edge

    def area(self):
        return sqrt(3) * self.lato * self.lato / 4



class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.punto1 = p1
        self.punto2 = p2
        self.punto3 = p3
        self.punto4 = p4

    def area(self):
        return (abs(
            self.punto1.ascissa * self.punto2.ordinata
            + self.punto2.ascissa * self.punto3.ordinata
            + self.punto3.ascissa * self.punto4.ordinata
            + self.punto4.ascissa * self.punto1.ordinata
            - self.punto2.ascissa * self.punto1.ordinata
            - self.punto3.ascissa * self.punto2.ordinata
            - self.punto4.ascissa * self.punto3.ordinata
            - self.punto1.ascissa * self.punto4.ordinata)) / 2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.punto1 = p1
        self.punto2 = p2
        self.punto4 = p4

    def area(self):
        return abs((self.punto2.ascissa - self.punto1.ascissa) * (self.punto4.ordinata - self.punto1.ordinata) - (
                    self.punto2.ordinata - self.punto1.ordinata) * (self.punto4.ascissa - self.punto1.ascissa))


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self.punto = p1
        self.base = base
        self.altezza = height

    def area(self):
        return self.base * self.altezza


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)



#include "shape.h"
#include <math.h>
#include <cmath>

using namespace std;

namespace ShapeLibrary {
    Point::Point(const double& x, const double& y)
    {
       ascissa=x;
       ordinata=y;
    }
    Point::Point(const Point& puntoDaCopiare)
    {
       this->ascissa=puntoDaCopiare.ascissa;
       ordinata=puntoDaCopiare.ordinata;// sto assegnando al punto che sto creando l'ordinata del punto da copiare che è già stato creato (che esiste già)
    }
    Ellipse::Ellipse(const Point& center, const int& a, const int& b): centro(center) //per entrare nel corpo di un costruttore tutte le variabili devono essere inizializzate, se non lo sono, il compilatore usa il costruttore di default, ma se non è stato specificato, come nel nostro caso, bisogna dire quale costruttore usare in modo esplicito nella "constructor initialization list"
    {
        //centro = center; questo verrebbe segnato come warning da alcuni compilatori, crea un'altra variabile che nasconde la vera variabile centro
        semiasseMaggiore= a;
        semiasseMinore= b;
    }
    double Ellipse::Area() const
    {
        return semiasseMaggiore*semiasseMinore*M_PI;
    }
    Circle::Circle(const Point& center, const int& radius): Ellipse(center, radius, radius) // chiamo il costruttore di ellipse con i parametri giusti
    {}// non serve creare nuovi attributi o membri, come l'area

    Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3): punto1(p1), punto2(p2), punto3(p3)
    {}
    double Triangle::Area() const
    {
        return (std::abs((punto2.ascissa-punto1.ascissa) * (punto3.ordinata-punto1.ordinata) - (punto2.ordinata-punto1.ordinata) * (punto3.ascissa-punto1.ascissa)))/2;
    }
    TriangleEquilateral::TriangleEquilateral(const Point& p1, const int& edge): Triangle(p1, p1, p1), punto(p1) // eredita da triangolo e sovrascrive il metodo
    {
        lato=edge;
    }
    double TriangleEquilateral::Area() const
    {
        return sqrt(3)*lato*lato/4;
    }
    Quadrilateral::Quadrilateral(const Point& p1,const Point& p2, const Point& p3, const Point& p4): punto1(p1), punto2(p2), punto3(p3), punto4(p4)
    {}
    double Quadrilateral::Area() const
    {
        return (std::abs(punto1.ascissa*punto2.ordinata + punto2.ascissa*punto3.ordinata + punto3.ascissa*punto4.ordinata + punto4.ascissa*punto1.ordinata - punto2.ascissa*punto1.ordinata - punto3.ascissa*punto2.ordinata - punto4.ascissa*punto3.ordinata - punto1.ascissa*punto4.ordinata))/2;
    }
    Parallelogram::Parallelogram(const Point& p1, const Point& p2, const Point& p4): Quadrilateral(p1, p1, p1, p1), punto1(p1), punto2(p2), punto4(p4)
    {}
    double Parallelogram::Area() const
    {
        return std::abs((punto2.ascissa-punto1.ascissa) * (punto4.ordinata-punto1.ordinata) - (punto2.ordinata-punto1.ordinata) * (punto4.ascissa-punto1.ascissa));
    }
    Rectangle::Rectangle(const Point& p1, const int& base, const int& height): Parallelogram(p1, p1, p1), punto1(p1)
    {
        this->base=base;
        altezza=height;
    }
    double Rectangle::Area() const
    {
        return base*altezza;
    }
    Square::Square(const Point& p1, const int& edge): Rectangle(p1, edge, edge)
    {}

}

/*Point a = new Point(1,2);
Point b = new Point(a); sto copiando i valori di a dentro b */

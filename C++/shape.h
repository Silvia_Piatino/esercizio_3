#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double ascissa;
      double ordinata;
      Point(const double& x,
            const double& y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    public:
      int semiasseMaggiore;
      int semiasseMinore;
      Point centro;
      Ellipse(const Point& center, //ciò a cui punta non si può cambiare
              const int& a,
              const int& b);

      double Area()const;
  };

  class Circle : public Ellipse // specifico la classe padre da cui eredita tutti i membri, quindi non serve ridefinire centro e raggio
  {
    public:
      Circle(const Point& center,
             const int& radius);
      // double Area()const; non serve perché lo eredita dal padre
  };


  class Triangle : public IPolygon
  {
    public:
      Point punto1;
      Point punto2;
      Point punto3;
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area()const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      Point punto;
      int lato;
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    public:
      Point punto1;
      Point punto2;
      Point punto3;
      Point punto4;
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Point punto1;
      Point punto2;
      Point punto4;
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public Parallelogram
  {
    public:
      Point punto1;
      int base;
      int altezza;
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

  };
}

#endif // SHAPE_H
